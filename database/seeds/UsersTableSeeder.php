<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        foreach(range(0,1000) as $i){
            DB::table('user')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => "123456",
                'address' => $faker->address,
                'date_of_birth' => $faker->dateTimeThisCentury()->format('Y-m-d')
            ]);
        }
    }
}
